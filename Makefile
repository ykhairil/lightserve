.POSIX:
#compiler type
UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Darwin)
	CC := clang -arch x86_64
else
	CC := gcc
endif

#flags
CFLAGS := -W -O

#dirs
SDIR := LightServe

#sources
SRCS := $(SDIR)/mongoose.c $(SDIR)/resumeserver.c $(SDIR)/main.c  

#object list, change to .o
OBJS := $(SRCS:.c=.o)

#program
MAIN := resumesrv

.PHONY: depend clean

all: $(MAIN)
	@echo “Compiled ”

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) -o $(MAIN) $(OBJS)

#rule for building o files from c files
.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) *.o *~ $(MAIN)

depend: $(SRCS)
	make depend $^
#