//
//  restfulobj.c
//  MiniServer
//
//  Created by syed on 11/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "restfulobj.h"

enum res_type{REST_INT, REST_UINT, REST_FLOAT, REST_CHAR, REST_STRING};

struct rest_resource
{
    unsigned long id:4;
    char* name;
    char* value;
    unsigned long name_length:4;
    unsigned long value_length:4;
    enum res_type valuetype:4;
};

struct rest_resource* resource_array;
size_t resource_array_size;

void rest_init(unsigned long array_size)
{
    resource_array_size = array_size;
    resource_array = NULL;
    resource_array = calloc(resource_array_size, sizeof(resource_array));
    
}

void rest_free()
{
    free(resource_array);
}

int parse_request(const char* input_string, int input_size, char* output_string, int output_size)
{
    int result=0;
    
    //process RESTful request to retrieve objects and values
    //request should come in this form:
    // [domain]/[api-version]/[object[s]]/[key]/[object[s]]/key/
    
    int current_search_start_pos=0;
    int current_search_end_pos=0;
    int entity_counter=0;
    
    for (int i=0; i < input_size; ++i)
    {
        if (input_string[i]=='/')
        {
            current_search_end_pos = i;
            current_search_start_pos = i+1;
            
            if (entity_counter == 0)
            {
                //domain name
            }
            else if (entity_counter == 1)
            {
                //api version
            }
            else if (entity_counter == 2)
            {
                //object name
                //determine if object is single or plural
                //according to English grammar
                //to define how many subresults should
                //be generated
            }
            else
            {
                
            }
        }
    }
    
    
    return result;
}
