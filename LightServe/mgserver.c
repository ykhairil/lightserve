//
//  mgserver.c
//  MiniServer
//
//  Created by syed on 10/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "mgserver.h"
#include "mongoose.h"
#include "restfulobj.h"

struct cerpelai_item
{
    int id:4;
    char* name;
};

struct cerpelai_item* items;
int items_size;


const char* rest_api_root="/cerpelai-api/v1";
const char* rest_api_kill_server="/cerpelai-api/v1/kill";
const char* rest_api_list_of_items="/cerpelai-api/v1/items";
const char* rest_api_specific_item="/cerpelai-api/v1/item/";

const char* item_json_template="{ \"id\": %d, \"name\" : \"%s\" }";


bool should_run = true;
struct mg_serve_http_opts server_opts;

void init_data(int size)
{
    items = NULL;
    items = calloc(size, sizeof(items));
    for (int i=0; i < size; ++i)
    {
        items[i].id=i;
        char label[7]="Bola_0";
        label[5]+=i;
        items[i].name = calloc(strlen(label),sizeof(char));
        memcpy(items[i].name, label, strlen(label));
        //items[i].name[7]='\0';
    }
}

void free_data(int size)
{
    for (int i=0; i < size; ++i)
    {
        free(items[i].name);
    }
    free(items);
    items=NULL;
}

void trigger_loop_cancel()
{
    should_run = false;
}

size_t get_resource_id_from_uri(const char* uri_str, size_t uri_len)
{
    size_t number_str_len = 0;
    size_t last_slash_pos=0;
    size_t index = 0;
    for (size_t i=0; i < uri_len; ++i)
    {
        //number_str[i]=hm->uri.p[i];
        if (uri_str[i]=='/')
        {
            last_slash_pos=i;
        }
    }
    number_str_len = uri_len - last_slash_pos;
    char* number_str = calloc(number_str_len, sizeof(char*));
    memcpy(number_str, uri_str + last_slash_pos+1, number_str_len-1);
    index = strtoul(number_str, NULL, 10);
    free(number_str);
    return index;
}

void get_json_for_specific_item(struct mg_connection *nc, struct http_message *hm)
{
    /* Get form variables */
    size_t uri_size = hm->uri.len;
    size_t number_str_len = 0;
    size_t last_slash_pos=0;
    size_t index = 0;
    for (size_t i=0; i < uri_size; ++i)
    {
        //number_str[i]=hm->uri.p[i];
        if (hm->uri.p[i]=='/')
        {
            last_slash_pos=i;
        }
    }
    number_str_len = uri_size - last_slash_pos;
    char* number_str = calloc(number_str_len, sizeof(char*));
    memcpy(number_str, hm->uri.p + last_slash_pos+1, number_str_len-1);
    index = strtoul(number_str, NULL, 10);
    free(number_str);
    
    /* Send headers */
    mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
    
    /* Compute the result and send it back as a JSON object */
    if (index < items_size)
    {
        mg_printf_http_chunk(nc, item_json_template, items[index].id, items[index].name);
    }
    else
    {
        mg_http_send_error(nc, 404, "Invalid index");
    }
    
    mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
}

void put_specific_item(struct mg_connection *nc, struct http_message *hm)
{
    /* Get form variables */
    size_t uri_size = hm->uri.len;
    size_t number_str_len = 0;
    size_t last_slash_pos=0;
    size_t index = 0;
    for (size_t i=0; i < uri_size; ++i)
    {
        //number_str[i]=hm->uri.p[i];
        if (hm->uri.p[i]=='/')
        {
            last_slash_pos=i;
        }
    }
    number_str_len = uri_size - last_slash_pos;
    char* number_str = calloc(number_str_len, sizeof(char*));
    memcpy(number_str, hm->uri.p + last_slash_pos+1, number_str_len-1);
    index = strtoul(number_str, NULL, 10);
    free(number_str);
    
    /* Send headers */
    mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
    
    /* Compute the result and send it back as a JSON object */
    if (index < items_size)
    {
        mg_printf_http_chunk(nc, item_json_template, items[index].id, items[index].name);
    }
    else
    {
        mg_http_send_error(nc, 404, "Invalid index");
    }
    
    mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
}

void process_specific_item(struct mg_connection *nc, struct http_message *hm)
{
    if (mg_vcmp(&hm->method, "GET") == 0)
    {
        get_json_for_specific_item(nc, hm);
    }
    else if (mg_vcmp(&hm->method, "POST") == 0)
    {
        
    }
    else if (mg_vcmp(&hm->method, "UPDATE") == 0)
    {
        
    }
    else if (mg_vcmp(&hm->method, "DELETE") == 0)
    {
        
    }
}

void get_json_for_list_of_items(struct mg_connection *nc, struct http_message *hm)
{
    /* Get form variables */
    
    /* Send headers */
    mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
    
    /* Compute the result and send it back as a JSON object */
    char* json_array = (char*)calloc(512,sizeof(char));
    const char* array_head = "{ \"items\" : [ ";
    memcpy(json_array, array_head, strlen(array_head));
    unsigned long json_array_last_pos=strlen(array_head);
    
    for (int i=0; i < items_size; ++i)
    {
        char buf[512];
        int char_count=0;
        char_count = sprintf(buf, item_json_template,items[i].id, items[i].name);
        if (i != items_size-1)
        {
            buf[char_count]=',';
            memcpy(json_array+json_array_last_pos, buf, char_count+1);
            json_array_last_pos+=char_count;
            json_array_last_pos+=1;
        }
        else
        {
            memcpy(json_array+json_array_last_pos, buf, char_count);
            json_array_last_pos+=char_count;
        }
    }
    memcpy(json_array+json_array_last_pos, " ] }", 4);
    mg_printf_http_chunk(nc, json_array);
    mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
    free(json_array);
}


void get_json_for_numbers_call(struct mg_connection *nc, struct http_message *hm) {
    char n1[512], n2[512];
    double result;
    
    /* Get form variables */
    int res1=0;
    int res2=0;
    res1 = mg_get_http_var(&hm->query_string, "n1", n1, sizeof(n1));
    res2 = mg_get_http_var(&hm->query_string, "n2", n2, sizeof(n2));
    
    /* Send headers */
    mg_printf(nc, "%s", "HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n");
    
    /* Compute the result and send it back as a JSON object */
    result = strtod(n1, NULL) + strtod(n2, NULL);
    mg_printf_http_chunk(nc, "{ \"result\": %lf }", result);
    mg_send_http_chunk(nc, "", 0); /* Send empty chunk, the end of response */
}

void ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    
    switch (ev) {
        case MG_EV_HTTP_REQUEST:
            if (mg_vcmp(&hm->uri, "/api/v1/numbers") == 0)
            {
                get_json_for_numbers_call(nc, hm); /* Handle RESTful call */
            }
            else if (mg_vcmp(&hm->uri, rest_api_kill_server) == 0)
            {
                trigger_loop_cancel();
            }
            else if (mg_vcmp(&hm->uri, rest_api_list_of_items) == 0)
            {
                get_json_for_list_of_items(nc, hm);
            }
            else if (mg_vcmp(&hm->uri, rest_api_specific_item) > 0)
            {
                process_specific_item(nc, hm);
            }
            else if (mg_vcmp(&hm->uri, "/printcontent") == 0)
            {
                char buf[100] = {0};
                memcpy(buf, hm->body.p,
                       sizeof(buf) - 1 < hm->body.len ? sizeof(buf) - 1 : hm->body.len);
                printf("%s\n", buf);
            }
            else
            {
                mg_serve_http(nc, hm, server_opts); /* Serve static content */
            }
            break;
        
        default:
            break;
    }
}

int init_server_loop(const char* root_dir)
{
    struct mg_mgr server_manager;
    struct mg_connection* server_connection = NULL;
    struct mg_bind_opts bind_opts;
    const char *err_str=NULL;
    
    
    
    mg_mgr_init(&server_manager, NULL);
    if (root_dir!=NULL)
        server_opts.document_root = root_dir;
    
    memset(&bind_opts, 0, sizeof(bind_opts));
    bind_opts.error_string = &err_str;
    
    server_connection = mg_bind_opt(&server_manager, "1234", ev_handler, bind_opts);
    if (server_connection == NULL)
    {
        fprintf(stderr, "Error starting server on port %s: %s\n", "1234",
                *bind_opts.error_string);
        return 1;
    }
    
    mg_set_protocol_http_websocket(server_connection);
    server_opts.enable_directory_listing = "yes";
    items_size=5;
    //init_data(items_size);
    while (should_run)
    {
        mg_mgr_poll(&server_manager, 1000);
    }
    mg_mgr_free(&server_manager);
    //free_data(items_size);
    return 0;
}
