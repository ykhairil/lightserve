//
//  restfulobj.h
//  MiniServer
//
//  Created by syed on 11/10/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef restfulobj_h
#define restfulobj_h

#include <stdio.h>
#include <stdlib.h>

int parse_request(const char* input_string, int input_size, char* output_string, int output_size);

#endif /* restfulobj_h */
