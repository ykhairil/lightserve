//
//  resumeserver.h
//  LightServe
//
//  Created by yussairi on 15/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#ifndef resumeserver_h
#define resumeserver_h

#include <stdio.h>

int init_resume_server_loop(const char* root_dir);

#endif /* resumeserver_h */
