//
//  resumeserver.c
//  LightServe
//
//  Created by yussairi on 15/11/2018.
//  Copyright © 2018 yussairi. All rights reserved.
//

#include "resumeserver.h"
#include "mongoose.h"
#include <time.h>

#ifdef __linux__
# include <sys/sysinfo.h>
#endif

#ifdef __APPLE__
# include <mach/task.h>
# include <mach/mach_init.h>
#endif

#ifdef WIN32
#include <windows.h>
#include <Psapi.h>
#else
# include <sys/resource.h>
#endif

bool should_resume_server_run = true;
struct mg_serve_http_opts resume_server_opts;

char* cv_name=NULL;

typedef struct cv_education
{
    char* institute;
    char* cert;
    char* year;
}res_cv_education;

typedef struct cv_comment
{
    char* name;
    char* comment;
    char* timestr;
}res_cv_comment;

const char* rest_resume_main="/resume";
const char* rest_resume_kill="/kill";
const char* rest_resume_item_name="/resume/name";
const char* rest_resume_item_education="/resume/education";
const char* rest_resume_item_comment="/resume/comment";
const char* rest_resume_style_0="/resume/style/0";
const char* rest_resume_style_1="/resume/style/1";
const char* rest_resume_style_2="/resume/style/2";
const char* rest_favicon="/favicon.ico";
const char* rest_touchicon="/apple-touch-icon-precomposed.png";

const char* http_response_chunked_200="HTTP/1.1 200 OK\r\nTransfer-Encoding: chunked\r\n\r\n";
const char* http_response_chunked_201="HTTP/1.1 201 Created\r\nTransfer-Encoding: chunked\r\n\r\n";
const char* http_response_chunked_204="HTTP/1.1 204 No Content\r\nTransfer-Encoding: chunked\r\n\r\n";
const char* http_response_chunked_400="HTTP/1.1 400 Bad Request\r\nTransfer-Encoding: chunked\r\n\r\n";
const char* http_response_chunked_404="HTTP/1.1 404 Not Found\r\nTransfer-Encoding: chunked\r\n\r\n";
const char* http_response_chunked_413="HTTP/1.1 413 Payload Too Large\r\nTransfer-Encoding: chunked\r\n\r\n";

const char* html_resume_top="<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>My Resume</title>%s</head><body bgcolor=\"#AAAAFF\">";
const char* html_resume_link_style0="<a href=\"%s\">Styleless</a>&emsp;";
const char* html_resume_link_style1="<a href=\"%s\">Paperback</a>&emsp;";
const char* html_resume_link_style2="<a href=\"%s\">Hackerman</a>&emsp;";
const char* html_resume_style_null=" ";
const char* html_resume_style_bookman="<style>body{background-color: ivory; color: darkslategray;font-family: \"Bookman\", \"Times\", \"serif\";} input[type=text]{border: 1px solid gray; color: black; background-color: white; border-radius: 4px; padding: 8px 8px;} input[type=submit]{min-width:64px; width:25%%; border: 1px solid linen; border-radius: 4px;float:right;} input[type=submit]:hover {background-color:white;} label {text-align:right;display:inline-block;width:200px;} table{min-width:50%%; width:75%%;} a:link{text-decoration:none;}a:visited{color: gray;text-decoration:none;}a:hover{color: black;text-decoration:none;}a:active{color: darkslategray; text-decoration:underline;}</style>";
const char* html_resume_style_helvetica = "<style>body{background-color: black; color: aquamarine;font-family: \"Helvetica\", \"Times\", \"serif\";} input[type=text]{border: 1px solid green; color: lightgreen; background-color: black; border-radius: 4px; padding: 8px 8px;} input[type=submit]{background-color:black; color:aquamarine; min-width:64px; width:25%%; border: 1px solid yellow; border-radius: 4px;float:right;} input[type=submit]:hover {color: black; background-color:aquamarine;} label {text-align:right;display:inline-block;width:200px;} table{min-width:50%%; width:75%%;} a:link{text-decoration:none;}a:visited{color: gray;text-decoration:none;}a:hover{color: yellow;text-decoration:none;}a:active{color: aquamarine; text-decoration:underline;}</style>";
const char* html_resume_basic_heading="<center><h2>Basic Information</h2></center><br>";
const char* html_resume_common_table_start = "<table align=\"center\">";
const char* html_resume_common_table_end = "</table>";
const char* html_resume_common_row_start = "<tr>";
const char* html_resume_common_row_end = "</tr>";
const char* html_resume_common_cell_start = "<td>";
const char* html_resume_common_cell_end = "</td>";

const char* html_resume_basic_info_autoyear="<b>Name : </b> Yussairi Khairil <br><b>Age : </b> %d <br><b>Ethnicity : </b> Malay <br><b>Citizenship : </b> Malaysian <br><b>Languages : </b> Malay, English, Malaysian Sign Language <br>";
const char* html_resume_picture = "<center><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABTADsDASIAAhEBAxEB/8QAHAAAAQUBAQEAAAAAAAAAAAAACQAFBgcIBAoD/8QAPBAAAQMDAwIDBQUFCAMAAAAAAQIDBAUGEQAHIRIxCEFhCRMUIlEyUoGRwRUjM0JxChYkcpKhotGxsuH/xAAbAQACAwEBAQAAAAAAAAAAAAAEBwMFCAAGAf/EADARAAEDAgMGBQMFAQAAAAAAAAECAxEABAUhMQYHEkFhcRMiUYGhFLHBCDJCUpHw/9oADAMBAAIRAxEAPwA/mlpaiO+2+Vs+GzaSuXxeFSbpNu29HMiXIUOpXcBKEJHKlrUUpSkcqUoDz11dUu1E7335sbbOYI9yXnalvyDghqpVaPFWc9vlcWDoSO+ftad6/H+9Mpe2EeRtLtq6tTIqZXmt1VvtkLHDIP0b5HbqVrKt0ezYvC6q89PelKqiVAuLlzHVPPOqPflWSSfrqpuccsLdzwnnQCPf/j0q6tdn764bDraMjpymvRtblz028KQzUKRUINUgSBlqTDfS+y4PRaSQfwOu7Xnl8PO2O9/gtqT1SsO9Khb09JDphlRdgTD9x1okpIP9PzxolfsovbGt+Na8axtruFS4Fn7qURJdahtOKDNZZSPnWz1fzp+0UgnKT1AYSrE1ji1peEi1WFRrQ19hN3ZgG5QUg6Vu/S0tLVhVdS0JP+0h3zVNxN7dldnY9Sdj0GoJfuGrw0HCZRDgZYK/qEhL+B9V5/oWzQqfbJbeOXZ7S7bSYJTLjDdlut/DIUC60tqY4pSlDuAoPt4z36TjQWJXX09o49OYSSO8ZVYYVa/UXjbJ0KhPaag2zVhxYAiw4zKGosdCUISBhKUjgDGtG0awEpgDPT09OcY7apjaI15isSFQ7OVUotOPzuvVFuKXsfcSrv8AjjWntu6/Sboowd+DlU95KQXYzxSos+mUnGkpY4Kvh+ouFCV5jPOnDe4gkK8JoGE9MqoDdKy2ww6tX8RIOBj7Q1g3xfQJGxW7Fj7uUFxyDXLVrkeQpxlXQpxLaw4AT9OCDnuFEdtEe38rcm4VKgWtbqqu8pJK5Tk5uOw1jPGSCSeOw5zrDPimtKobgbXVuDU6d8BMgvocSh1wKaUUrSD8/HykKIycfro7C7deHYmzcpI4FKCTB/tlmKCxJSb3DnWFDzBJIkemeVH8pdQRVqZHlNpUluU0l1IUOlQCgCMjyPOujXDbNXhV63oMymvx5VPlMIcjux1BTS0EDBSRxjGu7TpSZEik4QQYNLQ2fapWm9b3j5sO4mnnFJqVsyI6Wj9lK0yGgr8x7v8AL10SbQ0PbtzHNsN89lb3cckfs1xcqiPg/wAFBUpDiTnOAonntyEemqbaG2W/YOIbGcE/GfxV7s3dJYv0KcORyP4+YNOEXw90HciPL/btJjVRusxURpLKyopKUrSsAEEKR8yEn5SM451YdEt79myJ8OlMMpGFGQY7YbaCvupA4GPMDz/HURsnfmh2Xtu9WqjLbbp8dKVrdHzYCiAMfidULdfiW3Kve4as7tlbNUj23GhrciuyRIUJjqiohCQFJSkE5ORnOQOoDS/sB49slhKshy5+1MpTK1ukJTJ6CrsrmylA3cokaVOpEOdLoL75Z9831LjOuNhtZIzhYKAOkLCgMZAB51lvxY7b/wBy9salQ4qnEx3GVNguulahyDyoknHGrq8JfiwS8hmg3hSapQ7unp6HnJAdWxI6EklHUvkFPI5znn5j50T4+tz4d4bn0Gxqe847MuuqsQnRHwpxplTiQspHbOMgZ0LeNvXDttatmVJUIjkAdfivpdRboeW4IEGZHONKM9sdairH2etmkrV1uQqcyhw4wOvoBVgfTJOB9NSrTfalHNu2vTaeXHXjBitRy46rqW50ICcqPmTjJOnDThbSEpCRyFJp1xTiytWpJP8AtNN4X1Rdv6SqdXKtT6RDT3dlyEspPoCojJ9BzoXPt/8Axd2Lvp4O5Vu24p6pzqZU2Z6KiqOUMMe7Cgejq+fJyBnpAxk5PGmzcByqXlVnp1WqU6qTFJyp6S8p1ZH06lHIHoONUrf+yKty9salQ5Cf8RIQtxxBGArq8vyx+WtA4HuqskILt66VqjQZJBIMTqTB7dqy3jG/e5cuEN2LIbb4hJJlRAImNAJHfvWWPDZ7SREBq37Xux7rpapbPxK3B1JDQXnJHmRj1z+GiNXh4ndl7sTS3Wb4qLKYLaZBj0+f7lh1IA+3jkj0BHbQ8bc9j+N6bAh1ClVJ2j3BT0qhyGHUfuH3WVFsq45BV09XA7EcailZ9krvRbEzLdDfqKUnobfYlZyP8p/X17Z1lC+awUXS2/E8FxKlJUkj+QMH59DW2cPvcVFu28Eh1CkhSVA6pIBHXT1FbF8fHjv2/o9uUVNm1T46r0tz4guJV1KQgfLhavqo4GTngHWI/DHvhP3D8cFnXJJlPSHqbUE1GP1q+24wUqTkfdCscHvg6kdk+yF3PuittpuJCqXAWetwElSifUcc8nz1Zvhx8ESrH39qE7pUmn20BAZcUkAvqQkKWr/WtScn7mvebrcLwi82iZtraHSmVrVGXCnl6ZkgR1pcb3doMQw3Zm4vbk+FPkQAcytQMdcgCZ5RRftofbK2XdNws0m8KJULTkPK6EzG3fjYY+YJ+dQSlaOSP5FAZ7451rinXXS6vBalRalAkxn0hbbrUhC0OJPYgg4I0EO37MbuC5Z3Unqb90nt9So5/wDXVjxbVU1GbSFKSEpAxp949unw1a0qsHC16g+Ye0kEdZJ9qy/s/v6xFpCkYkyHvQg8B6zCSD0gD3qVzGkGMc+SD/4/+6YZcPpbizo5/ee6SVfRYIH/AHp/qDaJURbKnClTw6MjuB6aaV27VIUJmPFlwVMspDYEiMpaukAAcpWnyH0175hYSMzSJeSpRyqd+G9ymxLnkQnvcJZrCw6gEdnwB1D+pSP9taIYoTMQJQllOsQ183JbkiNVqW43I+DKnnqew2ltUpYSoAJWsq6SCcjBTnGCcE6vDYbxYxb0p8OHVZHwtQcRlKnQW/eD1CuUny+mdZp3qbr3G7h3HsORxtq8zgGqVczGsHUnODJ002xuN3vWl3YMbN4s4EPtwhsmYWkftTOgUB5QNFAADzSKtjcKBTaDbcipSm20fCoKkpPHWrBwPXnWQatH+AoU6Y02n3sorc7cqUtRP66sXxPb2N1H3cUvPfAMvpDDTCsOTnUnq6UjzB6cHyCeonVEWPsw3Mk1StzjOjyKkpTyUNTnklrJJ46VD647aYW5vYYYRYKxS6HA49wwIzCdQO51PpAnOaVP6gt4jOOYinBrJZLNuVSRmFOaEjonMA8yVcoNSWxaSmlKcSr+M4nq59CP+9TEMJI5Ce2mqgWtHpTaVMfELWpGOt55bqvzUTpxjuKWwk9uORjTZuXA4viBrPDXkEGuWarM9v1KdO3vFFAOeeOfy0tLQ7miamQda4HuS4nyC1/rpgrlGizwy29HbcQ48lSgR3JKcn8eog47gkduNLS1OzrFDLUUq4k65femuiIFYvGuTJQ9/Jp5MaMtXPuGyAVBI7DJAye5wNTSEgJpLKQPl6Ej/jpaWprzUDt9hXMklUnr+aUNRRTW8fQfrr4NqISeT3Pn66WloXmakc5dhX//2Q==\"></center>";
const char* html_resume_education_heading="<center><h2>Education</h2></center><br>";
const char* html_resume_education_table_info1="<b>Masters of Computer Science</b>, Universiti Malaya (2016)<br>";
const char* html_resume_education_table_info2="<b>Bachelor Degree in Computer Science</b>, Universiti Teknologi Malaysia (2008)<br>";
const char* html_resume_education_table_info3="<b>Diploma in Computer Science</b>, Universiti Teknologi Malaysia (2004)<br>";
const char* html_resume_comment_form="<form action=\"/resume/comment\" method=\"post\"><label>Name:</label><input type=\"text\" name=\"name\" placeholder=\"Your name\"><br><label>Comment:</label><input type=\"text\" name=\"comment\" placeholder=\"Your stuffs\" size=\"64\"><br> <right><input type=\"submit\" value=\"Submit\"></right> </form>";
const char* html_resume_skill_heading="<center><h2>Professional Skills</h2></center><br>";
const char* html_resume_skill_info1="<b>Programming languages: </b> C, C++, Objective-C, Java, C#, HTML, CSS, Javascript<br>";
const char* html_resume_skill_info2="<b>IDE tools: </b> Visual Studio, Eclipse, XCode, Android Studio<br>";
const char* html_resume_skill_info3="<b>Frameworks/libraries: </b> Microsoft STL, Boost, OpenCV, Direct3D, OpenGL, MFC, Apache POI, Android NDK, JNI, CryptoPP, dtrace, socket, mongoose, sqlite<br>";
const char* html_resume_skill_info4="<b>Database systems: </b> MySQL, MS-SQL, SQLite<br>";
const char* html_resume_skill_info5="<b>CB/CI tools: </b> Subversion, Git, Mercurial, IncrediBuild, CruiseControl, Jenkins, CMake, make<br>";
const char* html_resume_skill_info6="<b>Platforms: </b> Windows, OSX, Android, iOS<br>";
const char* html_resume_comment_heading="<hr><center><h1>Comments</h1></center><br>";
const char* html_resume_comment_table_empty_placeholder="<center>No comments available yet<center><br>";
const char* html_resume_comment_table_repeats="%s <br> -- <b>%s</b> <i>%s</i><br>";
const char* html_resume_career_heading="<center><h2>Career History</h2></center><br>";
const char* html_resume_career_summary="My career history is a bit too long to fit here. For more information, head to my <a href=\"https://www.linkedin.com/in/yussairi-khairil-ba29b114a\">LinkedIn profile</a>.";
const char* html_resume_about_heading="<center><h2>About Me</h2></center><br>";
const char* html_resume_about_summary="Quiet. Experiments a lot either in coding, farming, or cooking. An art man in a ruthless mathematical world. Strong against provocations, gossips, and stress. Weak against small furry fuzzy things &#x1F408 , chocolates, and spreadsheets.";
const char* html_resume_site_heading="<center><h2>About the Site</h2></center><br>";
const char* html_resume_site_summary="This site is intended to be a portfolio of some sort, demonstrating how a web server application can be done equally well using native C without need for bulky frameworks and dumbfounding literature of site artifacts. This site runs on a %s machine and is currently using %u KB out of %u KB total memory. This site does not collect any data except for debugging purposes, and the site's server code is publicly available at my Bitbucket Git repository to double check the fact.";
const char* html_resume_tracker="<hr><center>%d visits since %s</center>";
const char* html_resume_bottom="</body></html>";


const char* json_comment_obj = "{ \"id\":%d, \"name\":\"%s\", \"comment\":\"%s\", \"time\":\"%s\" }";

const char* log_connection = "%s - %s connected\n";
const char* log_request_normal = "%s - %s requests %s ";
const char* log_request_abnormal = "%s - %s tried requesting %s ";
char log_start_time[80];

//server-side data
res_cv_comment* cv_comment_list=NULL;
int cv_comment_list_max = 5;
int cv_comment_list_currentsize = -1;

int page_visit_counter=0;
int page_style_index = 0;

const char* get_os_name()
{
#ifdef WIN32
    return "Windows";
#elif defined(__APPLE__)
    return "OSX";
#elif __linux__
    return "Linux";
#else
    return "unknown OS";
#endif
}

unsigned long long get_total_memory()
{
#ifdef WIN32	
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof (statex);
	GlobalMemoryStatusEx(&statex);
	return statex.ullTotalPhys;
#elif defined(__APPLE__)
	
	return 0;
#elif defined(__linux__)
    struct sysinfo si;
    int error = sysinfo(&si);
    return si.totalram;
#else
	return 0;
#endif
}

unsigned int get_process_virtual_memory_usage()
{
#ifdef WIN32
    PROCESS_MEMORY_COUNTERS_EX pmc;
    GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
    SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
    SIZE_T physMemUsedByMe = pmc.WorkingSetSize;
    return virtualMemUsedByMe;
#elif defined(__APPLE__)
    // Inspired by:
    // http://miknight.blogspot.com/2005/11/resident-set-size-in-mac-os-x.html
    struct task_basic_info t_info;
    mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
    task_t task = MACH_PORT_NULL;
    if (task_for_pid(current_task(), getpid(), &task) != KERN_SUCCESS)
    {
        abort();
    }
    else
    {
        task_info(task, TASK_BASIC_INFO, (task_info_t)&t_info, &t_info_count);
    }
    unsigned int virtmem = (int)t_info.virtual_size;
    unsigned int psize = getpagesize();
    return ( virtmem*psize/1024 );
#elif defined(__linux__)
    struct rusage rusage;
    getrusage( RUSAGE_SELF, &rusage );
    return (size_t)rusage.ru_maxrss;
#else
    return 0;
#endif
}

void free_resume_server_resources()
{
    
    if (cv_comment_list != NULL)
    {
        for (int i=0; i < cv_comment_list_currentsize; ++i)
        {
            if (cv_comment_list[i].name != NULL)
                free(cv_comment_list[i].name);
            if (cv_comment_list[i].comment != NULL)
                free(cv_comment_list[i].comment);
            if (cv_comment_list[i].timestr != NULL)
                free(cv_comment_list[i].timestr);
            cv_comment_list[i].name = NULL;
            cv_comment_list[i].comment = NULL;
            cv_comment_list[i].timestr = NULL;
        }
    }
}

void kill_resume_server()
{
    should_resume_server_run = false;
}

void log_client_device_info(struct http_message *hm)
{
    
    struct mg_str hdr = *mg_get_http_header(hm, "User-Agent");
    char headerbuf[512]={0};
    memcpy(headerbuf, hdr.p, hdr.len);
    
    //get browser app
    char* hdr_ptr = NULL;
	if ((hdr_ptr = strstr(headerbuf, "Edge")) != NULL)
	{
		printf("using MS Edge ");
	}
	else if ((hdr_ptr = strstr(headerbuf, "Chrome")) != NULL)
	{
		printf("using Google Chrome ");
	}
	else if ((hdr_ptr = strstr(headerbuf, "Safari")) != NULL)
	{
		printf("using Safari ");
	}       
    else if ((hdr_ptr = strstr(headerbuf, "Firefox")) != NULL)
    {
        printf("using Mozilla Firefox ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "OPR")) != NULL)
    {
        printf("using Opera ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "MSIE")) != NULL)
    {
        printf("using MS Internet Explorer ");
    }    
    else if ((hdr_ptr = strstr(headerbuf, "Trident")) != NULL)
    {
        printf("using MS Edge ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "FBAN")) != NULL)
    {
        printf("using Facebook ");
    }
    
    //get platforms
    hdr_ptr = NULL;
    if ((hdr_ptr = strstr(headerbuf, "Windows")) != NULL)
    {
        printf("on Windows ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "Macintosh")) != NULL)
    {
        printf("on Mac OSX ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "Android")) != NULL)
    {
        printf("on Android ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "X11")) != NULL)
    {
        printf("on Linux (X11) ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "Linux")) != NULL)
    {
        printf("on Linux ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "iPhone")) != NULL)
    {
        printf("on iOS (iPhone) ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "iPad")) != NULL)
    {
        printf("on iOS (iPad) ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "Symbian")) != NULL)
    {
        printf("on Symbian ");
    }
    else if ((hdr_ptr = strstr(headerbuf, "Blackberry")) != NULL)
    {
        printf("on Blackberry OS ");
    }
    printf("\n");
}

#pragma mark HTML rendering segment

void render_default_page(struct mg_connection *nc, struct http_message *hm)
{
    mg_printf(nc, "%s", http_response_chunked_200);
    mg_printf_http_chunk(nc, "<html><head></head><body>We have no idea what you're trying to do</body></html>");
    /* Send empty chunk, the end of response */
    mg_send_http_chunk(nc, "", 0);
}

void render_page(struct mg_connection *nc, struct http_message *hm)
{
    mg_printf(nc, "%s", http_response_chunked_200);
    mg_printf_http_chunk(nc, "<html><head></head><body>Test</body></html>");
    /* Send empty chunk, the end of response */
    mg_send_http_chunk(nc, "", 0);
}

void render_resume_page(int style_id, struct mg_connection *nc, struct http_message *hm)
{
    time_t t = time(NULL);
    struct tm time_str = *localtime(&t);
    int auto_age = (time_str.tm_year + 1900)-1982;
    
    mg_printf(nc, "%s", http_response_chunked_200);
    //style config
    if (style_id==0)
        mg_printf_http_chunk(nc, html_resume_top, html_resume_style_null);
    else if (style_id==1)
        mg_printf_http_chunk(nc, html_resume_top, html_resume_style_bookman);
    else if (style_id==2)
        mg_printf_http_chunk(nc, html_resume_top, html_resume_style_helvetica);
    //style changer
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_link_style0, rest_resume_style_0);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_link_style1, rest_resume_style_1);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_link_style2, rest_resume_style_2);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    //basic info
    mg_printf_http_chunk(nc, html_resume_basic_heading);
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_picture);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_basic_info_autoyear, auto_age);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    //education
    mg_printf_http_chunk(nc, html_resume_education_heading);
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_education_table_info1);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_education_table_info2);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_education_table_info3);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    //skills
    mg_printf_http_chunk(nc, html_resume_skill_heading);
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_skill_info1);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_skill_info2);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_skill_info3);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_skill_info4);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_skill_info5);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_skill_info6);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    
    mg_printf_http_chunk(nc, html_resume_career_heading);
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_career_summary);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    
    mg_printf_http_chunk(nc, html_resume_about_heading);
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_about_summary);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    
    mg_printf_http_chunk(nc, html_resume_site_heading);
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
	mg_printf_http_chunk(nc, html_resume_site_summary, get_os_name(), get_process_virtual_memory_usage() / 1024, get_total_memory() / 1024);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    
    if (cv_comment_list != NULL)
    {
		mg_printf_http_chunk(nc, html_resume_comment_heading);
		mg_printf_http_chunk(nc, html_resume_common_table_start);
        
        if (cv_comment_list_currentsize > 0)
        {
            for (int i=0; i < cv_comment_list_currentsize; ++i)
            {
                mg_printf_http_chunk(nc, html_resume_common_row_start);
                mg_printf_http_chunk(nc, html_resume_common_cell_start);
                mg_printf_http_chunk(nc, html_resume_comment_table_repeats, cv_comment_list[i].comment, cv_comment_list[i].name, cv_comment_list[i].timestr);
                mg_printf_http_chunk(nc, html_resume_common_cell_end);
                mg_printf_http_chunk(nc, html_resume_common_row_end);
            }
        }
		mg_printf_http_chunk(nc, html_resume_common_table_end);
    }
    else
    {
		mg_printf_http_chunk(nc, html_resume_comment_heading);
        mg_printf_http_chunk(nc, html_resume_comment_table_empty_placeholder);
    }
    
    mg_printf_http_chunk(nc, html_resume_common_table_start);
    mg_printf_http_chunk(nc, html_resume_common_row_start);
    mg_printf_http_chunk(nc, html_resume_common_cell_start);
    mg_printf_http_chunk(nc, html_resume_comment_form);
    mg_printf_http_chunk(nc, html_resume_common_cell_end);
    mg_printf_http_chunk(nc, html_resume_common_row_end);
    mg_printf_http_chunk(nc, html_resume_common_table_end);
    
    
    mg_printf_http_chunk(nc, html_resume_tracker, page_visit_counter, log_start_time);
    mg_printf_http_chunk(nc, html_resume_bottom);
    /* Send empty chunk, the end of response */
    mg_send_http_chunk(nc, "", 0);
}

#pragma mark comment segment

void delete_comment(int object_id, struct mg_connection *nc, struct http_message *hm)
{
    //simplest way is to move pointer of objects after the deleted object
    //closer to the deleted position
    if (cv_comment_list==NULL)
    {
        mg_printf(nc, "%s", http_response_chunked_204);
        mg_send_http_chunk(nc, "", 0);
    }
    else
    {
        if (object_id < cv_comment_list_currentsize || object_id > cv_comment_list_max)
        {
            free(cv_comment_list[object_id].name);
            free(cv_comment_list[object_id].comment);
            free(cv_comment_list[object_id].timestr);
            cv_comment_list[object_id].name = NULL;
            cv_comment_list[object_id].comment = NULL;
            cv_comment_list[object_id].timestr = NULL;
            //move objects behind the deleted object, forward into the 'hole'
            for (int i=object_id+1; i < cv_comment_list_currentsize; ++i)
            {
                cv_comment_list[i-1].name = calloc(strlen(cv_comment_list[i].name)+1, sizeof(char));
                cv_comment_list[i-1].comment = calloc(strlen(cv_comment_list[i].comment)+1, sizeof(char));
                cv_comment_list[i-1].timestr = calloc(strlen(cv_comment_list[i].timestr)+1, sizeof(char));
                memcpy(cv_comment_list[i-1].name, cv_comment_list[i].name,strlen(cv_comment_list[i].name));
                memcpy(cv_comment_list[i-1].comment, cv_comment_list[i].comment,strlen(cv_comment_list[i].comment));
                memcpy(cv_comment_list[i-1].timestr, cv_comment_list[i].timestr,strlen(cv_comment_list[i].timestr));
                free(cv_comment_list[i].name);
                free(cv_comment_list[i].comment);
                free(cv_comment_list[i].timestr);
                cv_comment_list[i].name = NULL;
                cv_comment_list[i].comment = NULL;
                cv_comment_list[i].timestr = NULL;
            }
            --cv_comment_list_currentsize;
            mg_printf(nc, "%s", http_response_chunked_200);
            mg_send_http_chunk(nc, "", 0);
        }
        else
        {
            mg_printf(nc, "%s", http_response_chunked_404);
            mg_send_http_chunk(nc, "", 0);
        }
    }
}


void put_comment(int object_id, struct mg_connection *nc, struct http_message *hm)
{
    char new_name[128]={0};
    char new_comment[512]={0};
    //char new_time[80]={0};
    mg_get_http_var(&hm->body, "name", new_name, sizeof(new_name));
    mg_get_http_var(&hm->body, "comment", new_comment, sizeof(new_comment));
    
    if (cv_comment_list==NULL)
    {
        mg_printf(nc, "%s", http_response_chunked_204);
        mg_send_http_chunk(nc, "", 0);
    }
    else
    {
        if (object_id < cv_comment_list_currentsize || object_id > cv_comment_list_max)
        {
            if (strlen(new_name)==0 || strlen(new_comment)==0)
            {
                mg_printf(nc, "%s", http_response_chunked_400);
                mg_send_http_chunk(nc, "", 0);
            }
            else
            {
                free(cv_comment_list[object_id].name);
                free(cv_comment_list[object_id].comment);
                free(cv_comment_list[object_id].timestr);
                cv_comment_list[object_id].name = calloc(strlen(new_name)+1, sizeof(char));
                cv_comment_list[object_id].comment = calloc(strlen(new_comment)+1, sizeof(char));
                cv_comment_list[object_id].timestr = calloc(80+1, sizeof(char));
                memcpy(cv_comment_list[object_id].name, new_name,strlen(new_name));
                memcpy(cv_comment_list[object_id].comment, new_comment,strlen(new_comment));
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(cv_comment_list[object_id].timestr, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_printf(nc, "%s", http_response_chunked_200);
                mg_send_http_chunk(nc, "", 0);
            }
        }
        else
        {
            mg_printf(nc, "%s", http_response_chunked_404);
            mg_send_http_chunk(nc, "", 0);
        }
    }
}

void get_comment(struct mg_connection *nc, struct http_message *hm)
{
    
    if (cv_comment_list != NULL)
    {
        mg_printf(nc, "%s", http_response_chunked_200);
        mg_printf_http_chunk(nc,"{ \"comments\":[");
        for (int i = 0 ; i < cv_comment_list_currentsize; ++i)
        {
            mg_printf_http_chunk(nc, json_comment_obj, i, cv_comment_list[i].name, cv_comment_list[i].comment, cv_comment_list[i].timestr);
            if (i<cv_comment_list_currentsize-1)
                mg_printf_http_chunk(nc,",");
        }
        mg_printf_http_chunk(nc,"]}");
    }
    else
    {
        mg_printf(nc, "%s", http_response_chunked_404);
        //mg_printf_http_chunk(nc, "{ \"name\": \"NULL\" }");
    }
    /* Send empty chunk, the end of response */
    mg_send_http_chunk(nc, "", 0);
}

void post_comment(struct mg_connection *nc, struct http_message *hm, int do_refresh_page)
{
    char new_name[128]={0};
    char new_comment[512]={0};
    //char new_time[80]={0};
    mg_get_http_var(&hm->body, "name", new_name, sizeof(new_name));
    mg_get_http_var(&hm->body, "comment", new_comment, sizeof(new_comment));
    
    if (cv_comment_list==NULL)
    {
        //create the array for resource
        cv_comment_list = calloc(cv_comment_list_max, sizeof(res_cv_comment));        
        ++cv_comment_list_currentsize;
    }
    
    if (cv_comment_list_currentsize < cv_comment_list_max)
    {
        if (strlen(new_name)==0 || strlen(new_comment)==0)
        {
            mg_printf(nc, "%s", http_response_chunked_400);
            mg_send_http_chunk(nc, "", 0);
        }
        else
        {
            cv_comment_list[cv_comment_list_currentsize].name = calloc(strlen(new_name)+1, sizeof(char));
            cv_comment_list[cv_comment_list_currentsize].comment = calloc(strlen(new_comment)+1, sizeof(char));
            cv_comment_list[cv_comment_list_currentsize].timestr = calloc(80+1, sizeof(char));
            memcpy(cv_comment_list[cv_comment_list_currentsize].name, new_name,strlen(new_name));
			cv_comment_list[cv_comment_list_currentsize].name[strlen(new_name)] = 0;
            memcpy(cv_comment_list[cv_comment_list_currentsize].comment, new_comment,strlen(new_comment));
			cv_comment_list[cv_comment_list_currentsize].comment[strlen(new_comment)] = 0;

            time_t t = time(NULL);
            struct tm time_str = *localtime(&t);
            sprintf(cv_comment_list[cv_comment_list_currentsize].timestr, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
            cv_comment_list[cv_comment_list_currentsize].timestr[80] = 0;
            
            ++cv_comment_list_currentsize;
            if (do_refresh_page==0)
            {
            mg_printf(nc, "%s", http_response_chunked_201);
            mg_send_http_chunk(nc, "", 0);
            }
            else
            {
                render_resume_page(page_style_index, nc, hm);
            }
        }
    }
    else if (cv_comment_list_currentsize >= cv_comment_list_max - 1)
    {
        //pop off earliest one
        free(cv_comment_list[0].name);
        free(cv_comment_list[0].comment);
        free(cv_comment_list[0].timestr);
        cv_comment_list[0].name = NULL;
        cv_comment_list[0].comment = NULL;
        cv_comment_list[0].timestr = NULL;
        //move objects behind the deleted object, forward into the 'hole'
        for (int i=1; i < cv_comment_list_currentsize; ++i)
        {
            cv_comment_list[i-1].name = calloc(strlen(cv_comment_list[i].name)+1, sizeof(char));
            cv_comment_list[i-1].comment = calloc(strlen(cv_comment_list[i].comment)+1, sizeof(char));
            cv_comment_list[i-1].timestr = calloc(strlen(cv_comment_list[i].timestr)+1, sizeof(char));
            memcpy(cv_comment_list[i-1].name, cv_comment_list[i].name,strlen(cv_comment_list[i].name));
            memcpy(cv_comment_list[i-1].comment, cv_comment_list[i].comment,strlen(cv_comment_list[i].comment));
            memcpy(cv_comment_list[i-1].timestr, cv_comment_list[i].timestr,strlen(cv_comment_list[i].timestr));
            free(cv_comment_list[i].name);
            free(cv_comment_list[i].comment);
            free(cv_comment_list[i].timestr);
            cv_comment_list[i].name = NULL;
            cv_comment_list[i].comment = NULL;
            cv_comment_list[i].timestr = NULL;
        }
        
        cv_comment_list[cv_comment_list_currentsize-1].name = calloc(strlen(new_name)+1, sizeof(char));
        cv_comment_list[cv_comment_list_currentsize-1].comment = calloc(strlen(new_comment)+1, sizeof(char));
        cv_comment_list[cv_comment_list_currentsize-1].timestr = calloc(80+1, sizeof(char));
        memcpy(cv_comment_list[cv_comment_list_currentsize-1].name, new_name,strlen(new_name));
        cv_comment_list[cv_comment_list_currentsize-1].name[strlen(new_name)] = 0;
        memcpy(cv_comment_list[cv_comment_list_currentsize-1].comment, new_comment,strlen(new_comment));
        cv_comment_list[cv_comment_list_currentsize-1].comment[strlen(new_comment)] = 0;
        
        time_t t = time(NULL);
        struct tm time_str = *localtime(&t);
        sprintf(cv_comment_list[cv_comment_list_currentsize-1].timestr, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
        cv_comment_list[cv_comment_list_currentsize-1].timestr[80] = 0;
        
        if (do_refresh_page==0)
        {
            mg_printf(nc, "%s", http_response_chunked_201);
            mg_send_http_chunk(nc, "", 0);
        }
        else
        {
            render_resume_page(page_style_index, nc, hm);
        }
    }
    else
    {
        mg_printf(nc, "%s", http_response_chunked_413);
        mg_send_http_chunk(nc, "", 0);
    }
}


void access_comment(struct mg_connection *nc, struct http_message *hm)
{
    
    if (mg_vcmp(&hm->method, "GET") == 0)
    {
        get_comment(nc, hm);
    }
    else if (mg_vcmp(&hm->method, "POST") == 0)
    {
        post_comment(nc, hm, 1);
    }
    else if (mg_vcmp(&hm->method, "PUT") == 0)
    {
        //test with curl -X PUT -H "Content-Type: multipart/form-data;" -F "key1=val1" "YOUR_URI"
        //or curl -X PUT -H "Content-Type: multipart/form-data;" -d "institute=UTMK" -d "cert=degree" -d "year=2006" http://localhost:1234/resume/education/0
        //get id at end of resource
        //nominally, should search for / and get the number string afterwards
        //but i'm too lazy :O
        int object_id = -1;
        char c = hm->uri.p[hm->uri.len-1];
        object_id = atoi(&c);
        put_comment(object_id, nc, hm);
    }
    else if (mg_vcmp(&hm->method, "DELETE") == 0)
    {
        //test with curl -X DELETE http://localhost:1234/resume/education/0
        int object_id = -1;
        char c = hm->uri.p[hm->uri.len-1];
        object_id = atoi(&c);
        delete_comment(object_id, nc, hm);
    }
}

#pragma mark request handler

void resume_server_event_handler(struct mg_connection *nc, int ev, void *ev_data) {
    struct http_message *hm = (struct http_message *) ev_data;
    //print out the client IP
    
    switch (ev) {
        case MG_EV_HTTP_REQUEST:
            if (mg_vcmp(&hm->uri, rest_resume_main) == 0)
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_request_normal, timebuf, remoteAddr, rest_resume_main);
                log_client_device_info(hm);
                render_resume_page(page_style_index, nc, hm);
            }
            else if (mg_vcmp(&hm->uri, rest_resume_style_0) == 0)
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_request_normal, timebuf, remoteAddr, rest_resume_style_0);
                log_client_device_info(hm);
                page_style_index=0;
                render_resume_page(page_style_index, nc, hm);
            }
            else if (mg_vcmp(&hm->uri, rest_resume_style_1) == 0)
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_request_normal, timebuf, remoteAddr, rest_resume_style_1);
                log_client_device_info(hm);
                page_style_index=1;
                render_resume_page(page_style_index, nc, hm);
            }
            else if (mg_vcmp(&hm->uri, rest_resume_style_2) == 0)
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_request_normal, timebuf, remoteAddr, rest_resume_style_2);
                log_client_device_info(hm);
                page_style_index=2;
                render_resume_page(page_style_index, nc, hm);
            }
            else if (mg_vcmp(&hm->uri, rest_resume_kill) == 0)
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_request_normal, timebuf, remoteAddr, rest_resume_kill);
                log_client_device_info(hm);
                kill_resume_server();
            }
            else if (mg_vcmp(&hm->uri, rest_resume_item_comment) >= 0)
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_request_normal, timebuf, remoteAddr, rest_resume_item_comment);
                log_client_device_info(hm);
                access_comment(nc, hm);
            }
            else if (mg_vcmp(&hm->uri, "/printcontent") == 0)
            {
                char buf[100] = {0};
                memcpy(buf, hm->body.p,
                       sizeof(buf) - 1 < hm->body.len ? sizeof(buf) - 1 : hm->body.len);
                printf("%s\n", buf);
            }
            else if (mg_vcmp(&hm->uri, rest_favicon) >= 0)
            {
                //google chrome issue: it will try to request favicon.ico
                //after normal request, for some reason
                //not sure why
                //http://crbug.com/39402
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                char urireq[100]={0};
                snprintf(urireq, hm->uri.len+1, "%s", hm->uri.p);
                printf(log_request_abnormal, timebuf, remoteAddr, urireq);
                log_client_device_info(hm);
            }
            else if (mg_vcmp(&hm->uri, rest_touchicon) >= 0)
            {
                //safari issue: it will try to request icon
                //after normal request, for some reason
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                char urireq[100]={0};
                snprintf(urireq, hm->uri.len+1, "%s", hm->uri.p);
                printf(log_request_abnormal, timebuf, remoteAddr, urireq);
                log_client_device_info(hm);
            }
            else
            {
                //mg_serve_http(nc, hm, server_opts); /* Serve static content */
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                char urireq[100]={0};
                snprintf(urireq, hm->uri.len+1, "%s", hm->uri.p);
                printf(log_request_abnormal, timebuf, remoteAddr, urireq);
                printf("\n");
                render_default_page(nc, hm);
                
                
            }
            break;
        case MG_EV_ACCEPT:
            {
                char remoteAddr[100]={0};
                char timebuf[100]={0};
                time_t t = time(NULL);
                struct tm time_str = *localtime(&t);
                sprintf(timebuf, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
                
                mg_sock_to_str(nc->sock, remoteAddr, sizeof(remoteAddr), MG_SOCK_STRINGIFY_REMOTE | MG_SOCK_STRINGIFY_IP);
                printf(log_connection,timebuf, remoteAddr);
                ++page_visit_counter;
            }
            break;
            
        default:
            break;
            
    }
}


int init_resume_server_loop(const char* root_dir)
{
    struct mg_mgr server_manager;
    struct mg_connection* server_connection = NULL;
    struct mg_bind_opts bind_opts;
    const char *err_str=NULL;
    
    
    
    mg_mgr_init(&server_manager, NULL);
    if (root_dir!=NULL)
        resume_server_opts.document_root = root_dir;
    
    memset(&bind_opts, 0, sizeof(bind_opts));
    bind_opts.error_string = &err_str;
    
    server_connection = mg_bind_opt(&server_manager, "1234", resume_server_event_handler, bind_opts);
    if (server_connection == NULL)
    {
        fprintf(stderr, "Error starting server on port %s: %s\n", "1234",
                *bind_opts.error_string);
        return 1;
    }
	time_t t = time(NULL);
	struct tm time_str = *localtime(&t);
	sprintf(log_start_time, "%d-%d-%d %d:%d:%d", time_str.tm_year + 1900, time_str.tm_mon + 1, time_str.tm_mday, time_str.tm_hour, time_str.tm_min, time_str.tm_sec);
	printf("Server started at port 1234.\n");
    mg_set_protocol_http_websocket(server_connection);
    resume_server_opts.enable_directory_listing = "yes";
    
    while (should_resume_server_run)
    {
        mg_mgr_poll(&server_manager, 1000);
    }
	printf("Server stopped.");
    mg_mgr_free(&server_manager);
    free_resume_server_resources();
    return 0;
}
