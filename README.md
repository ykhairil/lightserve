# LightServe

An example of a lightweight server application made in C using Mongoose library

# Background

All Internet-based client-server application has the native 'socket' library at its heart, which serve as the OSI application layer most closest to metal. Mongoose is one of the few libraries that made socket programming slightly easier, with some useful features such as request queueing via threads and string management. 

## Implementation

An actual deployed implementation of this web server can be viewed at http://35.185.35.56/resume ,hosted on a Google Compute machine. Provided the machine is not turned off or if there has not been anything that could have prevented the web server from rerunning, the webserver should be serving website as normal.

# Deployment
## Debian
1. On a Debian VM, run `sudo apt install build-essential` and `sudo apt git` to install the toolchain necessary for building the server.
1. Clone this repo to a folder
1. In the folder, run `make` 
1. Run ./resumesrv to execute the server process
